public class BinaryTree {
    Node root;

    public BinaryTree() {
        this.root = null;
    }

    void inOrder(Node node){
        if(node == null){
            return;
        }
        inOrder(node.left);
        System.out.print(node.data + " ");
        inOrder(node.right);
    }

    void preOrder(Node node){
        if(node == null){
            return;
        }
        System.out.print(node.data + " ");
        preOrder(node.left);
        preOrder(node.right);
    }
    void postOrder(Node node){
        if(node == null){
            return;
        }
        preOrder(node.left);
        preOrder(node.right);
        System.out.print(node.data + " ");
    }

    public static void main(String[] args){
        BinaryTree binaryTree = new BinaryTree();
        binaryTree.root = new Node(10);
        binaryTree.root.left = new Node(20);
        binaryTree.root.right = new Node(30);
        System.out.println("IN-ORDER");
        binaryTree.inOrder(binaryTree.root);
        System.out.println();
        System.out.println("PRE-ORDER");
        binaryTree.preOrder(binaryTree.root);
        System.out.println();
        System.out.println("PRE-ORDER");
        binaryTree.postOrder(binaryTree.root);

    }

}
