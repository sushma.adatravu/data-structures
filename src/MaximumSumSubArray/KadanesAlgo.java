package MaximumSumSubArray;

import java.sql.SQLOutput;

public class KadanesAlgo {
    public static void main(String[] args){
        int[] a = {-13, -3, -25, -20, -3, -16, -23, -12, -5, -22, -15, -4, -7};
        int maxSum = Integer.MIN_VALUE;
        System.out.println(maxSum);
        int currentSum = 0;
        for(int i=0;i<a.length;i++){
            currentSum = currentSum + a[i];
            if(maxSum < currentSum){
                maxSum = currentSum;
            }
            if(currentSum < 0){
                currentSum =0;
            }
        }
        System.out.println("MaximumSumSubarray :" + maxSum);
    }
    //Time Complexity is O(n)
}
