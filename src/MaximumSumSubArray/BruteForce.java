package MaximumSumSubArray;

public class BruteForce {
    public static void main(String[] args){
        int[] a = {-5,4,6,-3,4,-1};
        int maxSum =0;
        for(int i=0;i<a.length;i++){
            int currentSum = 0;
            for(int j=i;j<a.length;j++){
                currentSum = currentSum + a[j];
                if(maxSum<currentSum){
                    maxSum = currentSum;
                }
            }
        }
        System.out.println("MAXSUM is " + maxSum);
    }
    //Time COmplexity is O(n2)
}
