public class ProductOfArray {
    public static int[] productExceptSelf(int[] arr){
        int[] prod = new int[arr.length];
        prod[0] = 1;
        for(int i=1;i<arr.length;i++){
            prod[i] = prod[i-1]*arr[i-1];
        }
        int temp = 1;
        for(int i=arr.length-2;i>=0;i--){
            prod[i] = prod[i] * arr[i+1]*temp;
            temp = temp * arr[i+1];
        }
        return prod;

    }
    public static void main(String[] args){
        int[] a = {5,1,4,2};
        int[] result = productExceptSelf(a);
        for(int i=0;i<result.length;i++){
            System.out.print(result[i] + " ");
        }
    }
}
