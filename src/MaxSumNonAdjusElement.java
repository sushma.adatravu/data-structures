public class MaxSumNonAdjusElement {
        int FindMaxSum(int arr[], int n)
        {
            int incl = arr[0];
            int excl = 0;
            int excl_new;
            int i;
            for (i = 1; i < n; i++)
            {
                excl_new = (incl > excl) ? incl : excl;
                incl = excl + arr[i];
                excl = excl_new;
            }
            return ((incl > excl) ? incl : excl);
        }
        public static void main(String[] args)
        {
            MaxSumNonAdjusElement sum = new MaxSumNonAdjusElement();
            int arr[] = new int[]{7, 10, 12, 7, 9, 14};
            System.out.println(sum.FindMaxSum(arr, arr.length));
        }
}
