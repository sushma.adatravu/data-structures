public class Stack {
    private int[] array;
    private int top;
    private int capacity;

    public Stack(int capacity) {
        this.array = new int[capacity];
        this.top = -1;
        this.capacity = capacity;
    }

    public void push(int item){
        if(top>=(capacity-1)){
            System.out.println("Stack overflow");
        }else {
            array[++top] = item;
        }
    }

    public int pop(){
        if(top<0){
            System.out.println("Stack underflow");
            return 0;
        }else {
            return array[top--];
        }
    }

    public int peek(){
        if(top<0){
            System.out.println("Stack underflow");
            return 0;
        }else {
            return array[top];
        }
    }

    boolean isEmpty(){
        return (top < 0);
    }

    public static void main(String[] args){
        Stack s = new Stack(5);
        s.push(10);
        s.push(20);
        s.push(30);
        s.push(40);
        s.push(50);
        System.out.println(s.pop());
        System.out.println(s.pop());
        System.out.println(s.peek());
        System.out.println(s.pop());
        System.out.println(s.pop());
        System.out.println(s.pop());
        System.out.println(s.top);
        System.out.println(s.isEmpty());

    }
}
