import java.util.ArrayList;

public class RootToLeafPathSumBinaryTree {
    static class Node{
        int data;
        Node right;
        Node left;
    }
    static Node newNode(int data){
        Node node = new Node();
        node.data = data;
        node.right = node.left = null;
        return node;
    }
    static void pathSum(Node root) {
        ArrayList<Integer> pathSum = new ArrayList();
        dfs(root, 0, pathSum);
        for(int num : pathSum)
        {
            System.out.print(num + " ");
        }
    }

    static void dfs(Node root, int sum, ArrayList<Integer> pathSum) {
        if (root == null) {
            return;
        }
        sum = sum + root.data;

        if (root.left == null &&
                root.right == null)
        {
            pathSum.add(sum);
            return;
        }
        dfs(root.left, sum, pathSum);
        dfs(root.right, sum, pathSum);
    }

    public static void main(String[] args){
        Node root = newNode(1);
        root.left = newNode(2);
        root.right = newNode(3);
        root.left.left = newNode(3);
        root.left.left.left = newNode(7);
        root.left.right = newNode(4);
        root.right.left = newNode(5);
        root.right.right = newNode(6);
        root.right.right.right = newNode(8);
        pathSum(root);
    }


}
