package NextGreaterElement;

public class BruteForce {
    public static void main(String[] args){
        int[] a = {2, 5, -3, -4, 6, 7, 2};
        for(int i=0;i<a.length;i++){
            int next = -1;
            for(int j= i+1;j<a.length;j++){
                if(a[j] > a[i]){
                    next = a[j];
                    break;
                }
            }
            a[i] = next;
        }

        for(int i=0;i<a.length;i++){
            System.out.print(a[i] + " ");
        }
    }
}
