package NextGreaterElement;

import java.util.Stack;

public class UsingStack {

    public static class stack {
        int[] array = new int[10];
        int top;

        public void push(int item) {
            if (top == 9)
            {
                System.out.println("Stack full");
            }
            else {
                array[++top] = item;
            }
        }

        public int pop(){
            if (top == -1)
            {
                System.out.println("Underflow error");
                return -1;
            }else{
                return array[top--];
            }
        }

        public boolean isEmpty(){
            if(top <0){
                return true;
            }else{
                return false;
            }
        }
    }

    public static void nextGreatElement(int[] a,int n){
        stack s = new stack();
        int next = 0;
        s.top = -1;
        s.push(a[0]);
        int element;
        for(int i=1;i<a.length;i++){
            next = a[i];
            if(s.isEmpty() == false) {
                element = s.pop();
                while(element < next){
                    System.out.println(element + " : " + next);
                    if(s.isEmpty() == true){
                        break;
                    }
                    element = s.pop();
                }

                if(element>next){
                    s.push(element);
                }
            }
            s.push(next);
        }
        while (s.isEmpty() == false)
        {
            element = s.pop();
            next = -1;
            System.out.println(element + " : " + next);
        }

    }

    public static void main(String[] args){
        int a[] = { 2, 5, -3, -4, 6, 7, 2 };
        int n = a.length;
        nextGreatElement(a,n);
    }
}
