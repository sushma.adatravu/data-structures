    class Node {

        int data;
        Node left, right;

        Node(int d)
        {
            data = d;
            left = right = null;
        }
    }

    class KthLargestInBST {
     Node root;
      KthLargestInBST()
        {
            root = null;
        }
        public void insert(int data)
        {
            this.root = this.insertRec(this.root, data);
        }
        Node insertRec(Node node, int data)
        {
            if (node == null) {
                this.root = new Node(data);
                return this.root;
            }

            if (data == node.data) {
                return node;
            }
            if (data < node.data) {
                node.left = this.insertRec(node.left, data);
            } else {
                node.right = this.insertRec(node.right, data);
            }
            return node;
        }

        public class count {
            int c = 0;
        }
        void kthLargestUtil(Node node, int k, count C)
        {
            if (node == null || C.c >= k)
                return;
            this.kthLargestUtil(node.right, k, C);
            C.c++;
            if (C.c == k) {
                System.out.println(k + "th largest element is " +
                        node.data);
                return;
            }
            this.kthLargestUtil(node.left, k, C);
        }
        void kthLargest(int k)
        {
            count c = new count();
            this.kthLargestUtil(this.root, k, c);
        }
        public static void main(String[] args)
        {
            KthLargestInBST tree = new KthLargestInBST();
            tree.insert(15);
            tree.insert(5);
            tree.insert(2);
            tree.insert(5);
            tree.insert(1);
            tree.insert(3);
            tree.insert(20);
            tree.insert(17);
            tree.insert(27);

            for (int i = 1; i <= 7; i++) {
                if(i ==3 ){
                    tree.kthLargest(i);
                }
            }
        }
    }
