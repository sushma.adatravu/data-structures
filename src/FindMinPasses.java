import java.util.ArrayDeque;
import java.util.Queue;

class Point
{
    int x, y;
    Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}

class FindMinPasses
{
    private static boolean isValid(int i, int j, int M, int N) {
        return (i >= 0 && i < M) && (j >= 0 && j < N);
    }
    private static int[] row = { -1, 0, 0, 1 };
    private static int[] col = { 0, -1, 1, 0 };

    private static boolean hasNegative(int[][] mat)
    {
        for (int i = 0; i < mat.length; i++)
        {
            for (int j = 0; j < mat[0].length; j++) {
                if (mat[i][j] < 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public static int findMinPasses(int[][] mat)
    {
        if (mat == null || mat.length == 0) {
            return 0;
        }
        int M = mat.length;
        int N = mat[0].length;
        Queue<Point> Q = new ArrayDeque<>();
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < N; j++)
            {
                if (mat[i][j] > 0) {
                    Q.add(new Point(i, j));
                }
            }
        }
        int passes = 0;
        while (!Q.isEmpty())
        {
            Queue<Point> q;
            q = new ArrayDeque<>(Q);
            Q.clear();
            while (!q.isEmpty())
            {
                int x = q.peek().x;
                int y = q.peek().y;
                q.poll();
                for (int k = 0; k < row.length; k++)
                {
                    if (isValid(x + row[k], y + col[k], M, N) &&
                            mat[x + row[k]][y + col[k]] < 0)
                    {
                        mat[x + row[k]][y + col[k]] = -mat[x + row[k]][y + col[k]];
                        Q.add(new Point(x + row[k], y + col[k]));
                    }
                }
            }
            passes++;
        }
        return hasNegative(mat) ? -1 : (passes - 1);
    }

    public static void main(String[] args)
    {
        int[][] mat =
                {
                        { 0, -1, -3, 2, 0},
                        { 1, -2, -5, -1, -3 },
                        { 3, 0, 0, -4, -1 }
                };

        int pass = findMinPasses(mat);
        if (pass != -1) {
            System.out.print("Minimum Passes are" + pass);
        }
        else {
            System.out.print("Invalid Input");
        }
    }
}
