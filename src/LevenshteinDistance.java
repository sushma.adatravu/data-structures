import java.util.Arrays;

public class LevenshteinDistance {
    static int calculate(String x, String y) {
        int[][] dp = new int[x.length() + 1][y.length() + 1];

        for (int i = 0; i <= x.length(); i++) {
            for (int j = 0; j <= y.length(); j++) {
                if (i == 0) {
                    dp[i][j] = j;
                }
                else if (j == 0) {
                    dp[i][j] = i;
                }
                else {
                    dp[i][j] = min(dp[i - 1][j - 1]
                                    + costOfSubstitution(x.charAt(i - 1), y.charAt(j - 1)),
                            dp[i - 1][j] + 1,
                            dp[i][j - 1] + 1);
                }
            }
        }
        return dp[x.length()][y.length()];
    }
    static int costOfSubstitution(char c1, char c2)
    {
        return c1 == c2 ? 0 : 1;
    }
    static int min(int... nums)
    {
        return Arrays.stream(nums).min().orElse(
                Integer.MAX_VALUE);
    }
    public static void main(String args[])
    {
        String s1 = "biting";
        String s2 = "mitten";
        System.out.println(calculate(s1, s2));
    }
}
