import java.util.LinkedList;
import java.util.Queue;

public class SumOfEachLevelBinaryTree {
    class Node {
        int data;
        Node left;
        Node right;
    }

    public void printSumAtEachLevel(Node node) {
            if (node == null) {
                return;
            }
            Queue<Node> queue = new LinkedList<Node>();
            queue.add(node);

            while (true) {
                int size = queue.size();
                if (size == 0) {
                    break;
                }
                int sum = 0;
                while (size > 0) {
                    Node node1 = queue.remove();
                    sum = sum + node1.data;

                    if (node1.left != null) {
                        queue.add(node1.left);
                    }
                    if (node1.right != null) {
                        queue.add(node1.right);
                    }
                    size--;
                }
                System.out.print(sum + " ");
            }
            return;
        }

        public Node createNewNode(int val) {
            Node newNode = new Node();
            newNode.data = val;
            newNode.left = null;
            newNode.right = null;
            return newNode;
        }

        public static void main(String[] args) {

            SumOfEachLevelBinaryTree sumateachlevel = new SumOfEachLevelBinaryTree();
            Node root = sumateachlevel.createNewNode(1);
            root.left = sumateachlevel.createNewNode(2);
            root.right = sumateachlevel.createNewNode(3);
            root.left.left = sumateachlevel.createNewNode(3);
            root.left.right = sumateachlevel.createNewNode(4);
            root.left.left.right = sumateachlevel.createNewNode(7);
            root.right.left = sumateachlevel.createNewNode(5);
            root.right.right = sumateachlevel.createNewNode(6);
            root.right.right.left = sumateachlevel.createNewNode(8);
            sumateachlevel.printSumAtEachLevel(root);
        }
    }