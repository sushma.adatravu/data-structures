public class DNFProblem {
    public static void main(String[] args){
        int[] a = {0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1};
        int n = a.length;
        int start = 0;
        int mid = 0;
        int high = n-1;
        while(mid<= high){
            int temp =0;
            if(a[mid] == 0){
                temp = a[mid];
                a[mid] = a[start];
                a[start] = temp;
                mid++;
                start++;
            }else if (a[mid] == 1){
                mid++;
            }else if(a[mid] == 2){
                temp = a[mid];
                a[mid] = a[high];
                a[high] = temp;
                high--;
            }
        }
      for(int i=0;i<a.length;i++){
          System.out.print(a[i] + " ");
      }
    }
}
