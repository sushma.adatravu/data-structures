public class SubArraySort {
    public static void main(String[] args){
        int[] a = {4, 8, 7, 12, 11, 9, -1, 3, 9, 16, -15, 51, 7};
        int start = 0;
        int end = 0;

        for(int i=0;i<a.length;i++){
            if(a[i] > a[i+1]){
                start = i;
                break;
            }
        }

        for(int j=a.length-1;j>0;j--){
            if(a[j] < a[j-1]){
                end = j;
                break;
            }
        }

        System.out.println("Start " + start + " End " + end);
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for(int i= start;i<=end;i++){
            if(a[i] > max){
                max = a[i];
            }
            if(a[i] < min){
                min = a[i];
            }
        }
        System.out.println("MAX " + max + " MIN " +min);

        for(int i=0;i<start;i++){
            if(a[i]> min){
                start=i;
            }
        }

        for(int i= a.length-1;i>end;i--){
            if(a[i]<max){
                end =i;
            }
        }
        System.out.println("Sub Array is: " + "[" + start + "," + end + "]");
    }
}
