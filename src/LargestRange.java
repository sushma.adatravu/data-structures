import java.util.HashSet;

public class LargestRange {
    public static void main(String[] args){
        int[] a = {19, -1, 18, 17, 2, 10, 3, 12, 5, 16, 4, 11, 8, 7, 6, 15, 12, 12, 2, 1, 6, 13, 14};
        HashSet<Integer> hs = new HashSet<Integer>();
        for(int i=0;i<a.length;i++){
            hs.add(a[i]);
        }
        int longestLength = 0;
        for(int i=0;i<a.length;i++){
            if(!hs.contains(a[i]-1)){
                int num = a[i];
                while(hs.contains(num)){
                    num++;
                }
                if(longestLength < num-a[i]){
                    longestLength = num-a[i];
                }
            }
        }
        System.out.println("Longest Length " + longestLength);
    }
}
